/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var appLicitaciones = angular.module('appLicitaciones', []);

appLicitaciones.controller('CtrlLicitaciones', ['$scope', '$http', '$log',
    function ($scope, $http, $log) {
        $log.debug('Definiendo controlador...');

        $scope.listaLicitaciones = [];

        $scope.listaTipos = [];

        $scope.listaCUCOP = [];

        $scope.titulo = "Agregar licitación";

        $scope.cargarLicitaciones = function () {
            $http.get('licitaciones')
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug("Datos: " + respuesta.data);
                        $scope.listaLicitaciones = respuesta.data;
                    });
        };

        $scope.cargarPartidas = function (id) {
            $http.get('licitaciones/' + id)
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug("Datos: " + respuesta.data);
                        $scope.nuevaLicitacion.partidas = respuesta.data.map(function(item) {
                            $scope.listaCUCOP.map(function(item2){
                                if (item.idCucop === item2.id) item.descripcion = item2.descripcion;
                            });
                            return item;
                        });
                    });
        };

        $scope.nuevaPartida = null;
        $scope.nuevaLicitacion = {};
        $scope.nuevaLicitacion.partidas = [];

        $scope.guardarLicitacion = function () {
            $log.debug("guardarLicitacion" + $scope.nuevaLicitacion);

//            if (!($scope.nuevoLibro.id) || $scope.nuevoLibro.id === 0) {
//
//                $http.post('libros', $scope.nuevoLibro)
//                        .then(function (respuesta) {
//                            $log.debug("POST exito");
//
//                            $scope.titulo = "Agregar libro";
//
//                            $scope.nuevoLibro = {};
//                            $scope.nuevoLibro.idEditorial = {};
//
//                            $scope.cargarLibros(); //opción 1: volver a consultar la base
////                        $scope.listaTareas.push(respuesta.data); //opción 2: agregar al listado
//                        }, function () {
//                            $log.debug("POST error");
//                        });
//            } else {
//                $http.put('libros/' + $scope.nuevoLibro.id, $scope.nuevoLibro)
//                        .then(function (respuesta) {
//                            $log.debug("PUT exito");
//
//                            $scope.titulo = "Agregar libro";
//
//                            $scope.nuevoLibro = {};
//                            $scope.nuevoLibro.idEditorial = {};
//
//                            $scope.cargarLibros(); //opción 1: volver a consultar la base
////                        $scope.listaTareas.push(respuesta.data); //opción 2: agregar al listado
//                        }, function () {
//                            $log.debug("PUT error");
//                        });
//            }
        };

        $scope.cargarCatalogos = function () {
            $http.get('tipoLicitacion')
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug("Datos: " + respuesta.data);
                        $scope.listaTipos = respuesta.data;
                    });
            $http.get('cucop')
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug("Datos: " + respuesta.data);
                        $scope.listaCUCOP = respuesta.data;
                        $scope.CUCOP = respuesta.data;
                    });
        };
        
        $scope.filtrarCucop = function(buscar){
            $scope.listaCUCOP = $scope.CUCOP.filter(function(item) {
               return (item.id.toString().indexOf(buscar) !== -1 ||
                       item.descripcion.toLowerCase().indexOf(buscar.toLowerCase()) !== -1); 
            });
        };

        $scope.agregarLicitacion = function () {
            //debugger;
            $scope.titulo = "Agregar licitación";

            $scope.nuevaLicitacion = {};
            $scope.nuevaLicitacion.partidas = [];
            $scope.nuevaPartida = null;
            $scope.buscar = "";
            $scope.listaCUCOP = $scope.CUCOP;
        };

        $scope.editarLicitacion = function (licitacion) {
            //debugger;
            $scope.titulo = "Editar licitacion";

            $scope.nuevaLicitacion = licitacion;
            $scope.nuevaLicitacion._vigenciaInicio = new Date(licitacion.vigenciaInicio);
            $scope.nuevaLicitacion._vigenciaFin = new Date(licitacion.vigenciaFin);
            
            $scope.cargarPartidas(licitacion.id);
            $scope.buscar = "";
            $scope.listaCUCOP = $scope.CUCOP;
        };
        
        $scope.agregarPartida = function(idc) {
            p = {};
            
            p.idCucop = Number(idc);
            p.idLicitacion = $scope.nuevaLicitacion.id;
            p.cantidad = 1;
            
            $scope.CUCOP.map(function(item){
                if (item.id === p.idCucop) p.descripcion = item.descripcion;
            });
            
            $scope.nuevaLicitacion.partidas.push(p);
            $scope.buscar = "";
            $scope.listaCUCOP = $scope.CUCOP;
        };
        
        $scope.borrarPartida = function(partida){
            $scope.nuevaLicitacion.partidas = $scope.nuevaLicitacion.partidas.filter(function(item){
                return !angular.equals(item, partida);
            });
        };

        $scope.borrarLicitacion = function (licitacionId) {
            
            if (!confirm('¿Estas seguro de elimnar el registro?')) return;
            
            //debugger;
//            $http.delete('licitaciones/' + licitacionId)
//                    .then(function (respuesta) {
//                        
//                        $scope.titulo = "Agregar licitación";
//                        
//                        $scope.nuevaLicitacion = {};
//
//                        $scope.cargarLicitaciones();
//                    });
        };

        $scope.cargarCatalogos();

        $scope.cargarLicitaciones();

        $log.debug('Controlador definido');
    }]);