<%-- 
    Document   : index
    Created on : 22/08/2018, 06:52:44 PM
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appLicitaciones">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.min.js"></script>
        <script src="js/CtrlLicitaciones.js"></script>
        <title>Licitaciones</title>
    </head>
    <body>
        <h1>Licitaciones</h1>
        <div ng-controller="CtrlLicitaciones">
            <table border="1">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Concepto</th>
                        <th>Vigencia inicio</th>
                        <th>Vigencia fin</th>
                        <th><input type="button" value="Nuevo" ng-click="agregarLicitacion()"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="t in listaLicitaciones">
                        <td>{{ t.folio}}</td>
                        <td>{{ t.concepto}}</td>
                        <td>{{ t.vigenciaInicio | date : "dd/MM/yyyy" }}</td>
                        <td>{{ t.vigenciaFin | date : "dd/MM/yyyy" }}</td>
                        <td><input type="button" value="Editar" ng-click="editarLicitacion(t)"> 
                            <input type="button" value="Borrar" ng-click="borrarLicitacion(t.id)"></td>
                    </tr>
                </tbody>
            </table></br></br>
            <h2>{{titulo}}</h2>
            <div id="forma">
                <table>

                    <input type="hidden" ng-model="nuevaLicitacion.id">
                    <tr>
                        <td><label>Folio:</label></td>
                        <td><input type="text" ng-model="nuevaLicitacion.folio"></td>
                        <td colspan="2" align="center">
                            <input type="button" value="Guardar" ng-click="guardarLicitacion()">
                        </td>
                    </tr>
                    <tr>
                        <td><label>Concepto:</label></td>
                        <td><textarea ng-model="nuevaLicitacion.concepto" style="width: 270px; height: 53px;"></textarea></td>
<!--                        <td><input type="text" ng-model="nuevaLicitacion.concepto"></td>-->
                    </tr>
                    <tr>
                        <td><label>Vigencia inicio:</label></td>
                        <td><input type="date" ng-model="nuevaLicitacion._vigenciaInicio" placeholder="yyyy-MM-dd"></td>
                    </tr>
                    </tr>
                    <tr>
                        <td><label>Vigencia fin:</label></td>
                        <td><input type="date" ng-model="nuevaLicitacion._vigenciaFin" placeholder="yyyy-MM-dd"></td>
                    </tr>
                    <tr>
                        <td><label>Tipo:</label></td>
                        <td><select ng-model="nuevaLicitacion.idTipoLicitacion" 
                                    ng-options="p.id as p.descripcion for p in listaTipos">
                            </select></td>
                    </tr>
                </table>
                <table>
                    <thead>
                        <tr>
                            <th>Partidas</th>
                            <th colspan="2">
                                <input type="text" ng-model="buscar" placeholder="Buscar..." 
                                       ng-change="filtrarCucop(buscar)">
                            </th>
                        </tr>
                        <tr>
                            <th colspan="3">
                                <select ng-model="nuevaPartida" style="width: 240px;"
                                    size="3">
                                    <option ng-repeat="p in listaCUCOP" 
                                            value="{{p.id}}">
                                        ({{p.id}}) {{p.descripcion}}
                                    </option>
<!--                                    ng-options="p.id as p.descripcion for p in listaCUCOP" size="3">-->
                                </select>
                            </th>
                            <th><input type="button" value="Agregar" 
                                       ng-disabled="!nuevaPartida"
                                       ng-click="agregarPartida(nuevaPartida)"></th>
                        </tr>
                        <tr>
                            <th>Partida</th>
                            <th>Descripción</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="d in nuevaLicitacion.partidas">
                            <td>{{d.idCucop}}</td>
                            <td>{{d.descripcion}}</td>
<!--                            <td>{{d.cantidad}}</td>-->
                            <td>
                                <input type="number" min="1" ng-model="d.cantidad" 
                                       style="width: 50px;">
                            </td>
                            <td><input type="button" value="Borrar" 
                                       ng-click="borrarPartida(d)"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>