/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Oscar
 */
@Entity
@Table(name = "LICITACIONES")
public class Licitacion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "folio")
    private String folio;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "concepto")
    private String concepto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "idTipoLicitacion")
    private Integer idTipoLicitacion;
    
    @Column(name = "vigenciaInicio")
    @Temporal(TemporalType.DATE)
    private Date vigenciaInicio;
    
    @Column(name = "vigenciaFin")
    @Temporal(TemporalType.DATE)
    private Date vigenciaFin;

    public Licitacion() {
    }

    public Licitacion(Integer id, String folio, String concepto, Integer idTipoLicitacion, Date vigenciaInicio, Date vigenciaFin) {
        this.id = id;
        this.folio = folio;
        this.concepto = concepto;
        this.idTipoLicitacion = idTipoLicitacion;
        this.vigenciaInicio = vigenciaInicio;
        this.vigenciaFin = vigenciaFin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    @Override
    public String toString() {
        return "Licitacion{" + "id=" + id + ", folio=" + folio + ", concepto=" + concepto + ", idTipoLicitacion=" + idTipoLicitacion + ", vigenciaInicio=" + vigenciaInicio + ", vigenciaFin=" + vigenciaFin + '}';
    }

    public Integer getIdTipoLicitacion() {
        return idTipoLicitacion;
    }

    public void setIdTipoLicitacion(Integer idTipoLicitacion) {
        this.idTipoLicitacion = idTipoLicitacion;
    }

    public Date getVigenciaInicio() {
        return vigenciaInicio;
    }

    public void setVigenciaInicio(Date vigenciaInicio) {
        this.vigenciaInicio = vigenciaInicio;
    }

    public Date getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(Date vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }
}
