/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Oscar
 */
@Entity
@Table(name = "LICITACIONESDETALLE")
public class LicitacionDetalle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "idLicitacion")
    private Integer idLicitacion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCucop")
    private Integer idCucop;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private Integer cantidad;

    public LicitacionDetalle() {
    }

    public LicitacionDetalle(Integer id, Integer idLicitacion, Integer idCucop, Integer cantidad) {
        this.id = id;
        this.idLicitacion = idLicitacion;
        this.idCucop = idCucop;
        this.cantidad = cantidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdLicitacion() {
        return idLicitacion;
    }

    public void setIdLicitacion(Integer idLicitacion) {
        this.idLicitacion = idLicitacion;
    }

    public Integer getIdCucop() {
        return idCucop;
    }

    public void setIdCucop(Integer idCucop) {
        this.idCucop = idCucop;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "LicitacionDetalle{" + "id=" + id + ", idLicitacion=" + idLicitacion + ", idCucop=" + idCucop + ", cantidad=" + cantidad + '}';
    }

    
}
