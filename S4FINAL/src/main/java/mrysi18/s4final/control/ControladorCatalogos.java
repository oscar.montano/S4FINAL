/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.control;

import java.util.List;
import mrysi18.s4final.dao.*;
import mrysi18.s4final.entidades.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Oscar
 */
@RestController
public class ControladorCatalogos {
    @Autowired
    CUCOPDAO cucopDAO;
    
    @Autowired
    TipoLicitacionDAO tipoLicitacionDAO;
    
    private final static Logger LOG = LoggerFactory.getLogger(ControladorCatalogos.class);
    
    @GetMapping("/cucop")
    public List<CUCOP> getAllCUCOP() {
        return cucopDAO.findAll();
    }
    
    @GetMapping("/cucop/{buscar}")
    public List<CUCOP> buscarCUCOP(@PathVariable("buscar") String buscar) {
        LOG.debug("GET {}", buscar);
//        return cucopDAO.buscarCUCOP(buscar);
        return cucopDAO.findByDescripcionContainingIgnoreCase(buscar);
    }
    
    @GetMapping("/tipoLicitacion")
    public List<TipoLicitacion> getAllTipoLicitacion() {
        return tipoLicitacionDAO.findAll();
    }
}
