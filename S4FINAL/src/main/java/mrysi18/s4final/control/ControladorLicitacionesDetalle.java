/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.control;

import java.util.List;
import mrysi18.s4final.dao.LicitacionDetalleDAO;
import mrysi18.s4final.entidades.LicitacionDetalle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Oscar
 */
@RestController
public class ControladorLicitacionesDetalle {
    @Autowired
    LicitacionDetalleDAO detalleDAO;
    
    private final static Logger LOG = LoggerFactory.getLogger(ControladorLicitacionesDetalle.class);
    
    @GetMapping("/licitaciones/{licitacion}")
    public List<LicitacionDetalle> getLicitacionDetalle(@PathVariable("licitacion") Integer licitacion) {
        return detalleDAO.buscarPorLicitacion(licitacion);
    }

}
