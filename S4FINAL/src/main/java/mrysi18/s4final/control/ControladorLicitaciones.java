/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.control;

import java.util.List;
import mrysi18.s4final.dao.LicitacionDAO;
import mrysi18.s4final.entidades.Licitacion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Oscar
 */
@RestController
public class ControladorLicitaciones {
    @Autowired
    LicitacionDAO licitacionDAO;
    
    private final static Logger LOG = LoggerFactory.getLogger(ControladorLicitaciones.class);

    @GetMapping("/licitaciones")
    public List<Licitacion> getAllLicitaciones() {
        return licitacionDAO.findAll();
    }
}
