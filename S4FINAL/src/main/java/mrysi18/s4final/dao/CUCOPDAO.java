/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.dao;

import java.util.List;
import mrysi18.s4final.entidades.CUCOP;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Oscar
 */
public interface CUCOPDAO extends JpaRepository<CUCOP, Integer>{    
//    @Query(value = "SELECT t.* FROM CUCOP t "
//            + "WHERE LOWER(descripcion) LIKE '%' || LOWER( ?1 ) || '%' "
//            + "OR CAST(id AS VARCHAR) LIKE '%' || ?1 || '%' ", nativeQuery = true)
//    List<CUCOP> buscarCUCOP(String buscar);
    List<CUCOP> findByDescripcionContainingIgnoreCase(@Param("buscar") String buscar);
}
