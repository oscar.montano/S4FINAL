/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.dao;

import mrysi18.s4final.entidades.TipoLicitacion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Oscar
 */
public interface TipoLicitacionDAO extends JpaRepository<TipoLicitacion, Integer> {
    
}
