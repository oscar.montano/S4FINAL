/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi18.s4final.dao;

import java.util.List;
import mrysi18.s4final.entidades.LicitacionDetalle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Oscar
 */
public interface LicitacionDetalleDAO extends JpaRepository<LicitacionDetalle, Integer> {
//    List<LicitacionDetalle> findById_licitacion(Integer licitacion);
    @Query("SELECT t FROM LicitacionDetalle t "
            + "WHERE t.idLicitacion = :licitacion")
    List<LicitacionDetalle> buscarPorLicitacion(@Param("licitacion") Integer licitacion);
}
